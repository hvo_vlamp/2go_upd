﻿using RestSharp;
using System;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.ServiceProcess;
using System.Net.Sockets;

namespace Updater2
{
    public partial class Updater : Form
    {
        public Updater()
        {
            InitializeComponent();
            // --- UPDATE 07.04.2020 / SILENT UPDATE FUNKTION ---
            if (Globals.Silent == true)
            //if (1 == 1)
            {
                string lcSilentInstall = SilentUpdate();
                if (lcSilentInstall == "OK")
                {
                    string lcPath = @"C:\2goup\";
                    string filepath = lcPath + "SILENTINSTALL.LOG";
                    using (StreamWriter sw = File.CreateText(filepath))
                    {
                        sw.WriteLine("HVO2go wurde erfolgreich im SILENT-INSTALL geupdatet");
                    }
                }
                System.Windows.Forms.Application.Exit();
                Environment.Exit(0);
            }
        }


        public string SilentUpdate()
        {
            string lcReturn = "FEHLER";
            string lcPath = @"C:\2goup\";

            if (Directory.Exists(lcPath) == false)
            {
                System.Windows.Forms.Application.Exit();
                return lcReturn;
            }

            // --- Eventuell alte Error.Log löschen ---
            string errorlog = lcPath + "ERROR.LOG";
            if (File.Exists(errorlog) == true)
            {
                File.Delete(errorlog);
            }

            string silentinstalllog = lcPath + "SILENTINSTALL.LOG";
            if (File.Exists(silentinstalllog) == true)
            {
                File.Delete(silentinstalllog);
            }



            // --- Port rausfinden ansonsten ERROR.LOG ablegen ---
            string lcPort = getPort();
            if (lcPort == "FEHLER")
            {
                // --- ERROR LOG ablegen --
                string filepath = lcPath + "ERROR.LOG";
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine("FEHLER : PORT KONNTE NICHT GEFUNDEN WERDEN");
                }
                System.Windows.Forms.Application.Exit();
                return lcReturn;
            }
            txtPort.Text = lcPort;

            linkURL.Text = Globals.Host + lcPort + "/servoy-admin/";

            // --- PFADE herausfinden und Prüfen ob alle hinterlegt worden sind ---
            cmdRepoPaths_Click(new object(), new EventArgs());


            if (lblServoyPath.Text == "FEHLER")
            {
                // --- ERROR LOG ablegen --
                string filepath = lcPath + "ERROR.LOG";
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine("FEHLER : SERVOY-PFAD KONNTE NICHT GEFUNDEN WERDEN");
                }
                System.Windows.Forms.Application.Exit();
                return lcReturn;
            }

            if (lblVELOpath.Text == "FEHLER")
            {
                // --- ERROR LOG ablegen --
                string filepath = lcPath + "ERROR.LOG";
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine("FEHLER : VELO-PFAD KONNTE NICHT GEFUNDEN WERDEN");
                }
                System.Windows.Forms.Application.Exit();
                return lcReturn;
            }

            if (lblJASPERpath.Text == "FEHLER")
            {
                // --- ERROR LOG ablegen --
                string filepath = lcPath + "ERROR.LOG";
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine("FEHLER : JASPER-PFAD KONNTE NICHT GEFUNDEN WERDEN");
                }
                System.Windows.Forms.Application.Exit();
                return lcReturn;
            }

            if (lblDATpath.Text == "FEHLER")
            {
                // --- ERROR LOG ablegen --
                string filepath = lcPath + "ERROR.LOG";
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine("FEHLER : DAT-PFAD KONNTE NICHT GEFUNDEN WERDEN");
                }
                System.Windows.Forms.Application.Exit();
                return lcReturn;
            }

            // --- Ist Version schon installiert? ----
            string vsinfopath       = lblServoyPath.Text + "application_server/vs.info";
            string vsinfopathNew    = lcPath + @"\UPD\vs.info";
            string installedVS      = "";
            string newVS            = "";
            int installedVERS       = 0;
            int newVERS             = 0;
                        
            if (File.Exists(vsinfopathNew) == true)
            {
                if (File.Exists(vsinfopath) == true)
                {
                    using (StreamReader sr = File.OpenText(vsinfopath))
                    {
                        installedVS = sr.ReadToEnd();
                        installedVERS = Int32.Parse(installedVS);
                    }

                    using (StreamReader sr2 = File.OpenText(vsinfopathNew))
                    {
                        newVS = sr2.ReadToEnd();
                        newVERS = Int32.Parse(newVS);
                    }

                    // --- Sind die Versionsnummern gleich ? ---
                    if (installedVERS >= newVERS)
                    {
                        System.Windows.Forms.Application.Exit();
                        return lcReturn;// --- Version schon installiert, Update abbrechen ---
                    }
                }
                // --- kein Aktuelles Versionsfile vorhanden, einfach updaten ---
            }

            // --- Update starten ---
            chkMan.Checked = true;

            button3_Click(new object(), new EventArgs());   // --- CHECK UPDATE 
            button1_Click_2(new object(), new EventArgs()); // --- BACKUP HVO/DAT
            cmdUpdate_Click(new object(), new EventArgs()); // --- RUN UPDATE
            //StopServoyService();      <== weil silentupdate nicht als admin gestartet werden kann ==> nicht über den dienst gehen (Update 21.12.2021)
            

            cmdCopyFiles_Click(new object(), new EventArgs());  // --- COPY FILES       // <== wird geprüpft obs nötig ist im silentinstall 
            if (AddLine() == false)// --- ADD EXTRA LINE FOR LONG FUNCTIONS (Update 15.07.2020)
            {
                MessageBox.Show("Konnte DISABLESCRIPTCOMPILE nicht anfügen!");
            }

            // --- update log4j vulnerability fix ---
            if (AddLineLOG4J() == false)// --- ADD EXTRA LINE FOR LONG FUNCTIONS (Update 13.12.2021)
            {
                MessageBox.Show("Konnte LOG4J Fix nicht anfügen!");
            }

            if (AddLineLOG4J_WRAPPER() == false)// --- ADD EXTRA LINE FOR LONG FUNCTIONS (Update 14.12.2021)
            {
                MessageBox.Show("Konnte LOG4J Fix nicht anfügen!");
            }
            //restartService("ServoyService");   // --- RESTART SERVER // DEACTIVATED BECAUSE OF RECHTE_BUG (Update 21.12.2021)
            button1_Click(new object(), new EventArgs()); // --- restart as admin page 
            chkMan.Checked = false;
            cmdStart.Enabled = false;
            lblUpdate.Text = "Update installiert!";

            // --- vs.info ablegen ---
            if (File.Exists(vsinfopathNew) == true)
            {
                if (File.Exists(vsinfopath) == true)
                {
                    File.Delete(vsinfopath);
                }
                File.Copy(vsinfopathNew, vsinfopath);
            }

            // --- Alles erfolgreich, UPDATER schließen ---
            lcReturn = "OK";
            return lcReturn;
        }


        private Boolean AddLine()
        {
            string lcFile = lblServoyPath.Text + @"\application_server\servoy.properties";

            if (File.Exists(lcFile) == false)
            {
                return false;
            }
            string lcText = File.ReadAllText(lcFile);
            if (lcText.ToLower().Contains("system.property.servoy.disablescriptcompile=true"))
            {
                return true;
            }
            else
            {
                using (StreamWriter sw = File.AppendText(lcFile))
                {
                    sw.WriteLine("system.property.servoy.disableScriptCompile=true");
                }
            }
            return true;
        }

        private Boolean AddLineLOG4J()
        {
            string lcFile = lblServoyPath.Text + @"\application_server\servoy.properties";

            if (File.Exists(lcFile) == false)
            {
                return false;
            }
            string lcText = File.ReadAllText(lcFile);
            if (lcText.ToLower().Contains("log4j2.formatmsgnolookups=true"))
            {
                return true;
            }
            else
            {
                using (StreamWriter sw = File.AppendText(lcFile))
                {
                    sw.WriteLine("log4j2.formatMsgNoLookups=true");
                }
            }
            return true;
        }
        
        private Boolean AddLineLOG4J_WRAPPER()
        {
            // in der wrapper conf, auch die LOG4j Lücke schließen 
            string lcFile = lblServoyPath.Text + @"\application_server\service\wrapper.conf";
            //string lcFile = @"C:\APPLICATION_SERVER_TEST\Servoy\application_server\service\wrapper.conf";



            if (File.Exists(lcFile) == false)
            {
                return false;
            }
            string lcText = File.ReadAllText(lcFile);
            if (lcText.ToLower().Contains("=-dlog4j2.formatmsgnolookups=true"))
            {
                return true;
            }
            else
            {
                // --- zähle die wrapper.java.additional. sätze um das in die neue zeile einzufügen ---
                int count = Regex.Matches(lcText, "wrapper.java.additional.").Count;
                count++; // hochzählen wegen neuem parameter
                using (StreamWriter sw = File.AppendText(lcFile))
                {
                    sw.WriteLine("wrapper.java.additional." + count.ToString() + "=-Dlog4j2.formatMsgNoLookups=true");
                }
            }
            return true;

        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        private Boolean AddLineEastwood()
        {
            string lcFile = lblServoyPath.Text + @"\application_server\servoy.properties";

            if (File.Exists(lcFile) == false)
            {
                return false;
            }
            string lcText = File.ReadAllText(lcFile);
            if (lcText.ToLower().Contains("velocityreport.serverurl="))
            {
                string[] lines = File.ReadAllLines(lcFile);
                int pos = 0;
                foreach (var line in lines)
                {
                    if (line.ToLower().Contains("velocityreport.serverurl="))
                    {
                        lines[pos] = "velocityreport.serverURL=" + GetLocalIPAddress() + ":" + txtPort;
                    }
                    pos = pos + 1;
                }

                File.WriteAllLines(lcFile, lines);
                return true;
            }
            else
            {
                using (StreamWriter sw = File.AppendText(lcFile))
                {
                    sw.WriteLine("velocityreport.serverURL=" + GetLocalIPAddress() + ":" + txtPort);
                }
            }
            return true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var lcPort = txtPort.Text;
            var lcURL = Globals.Host + lcPort + "/servoy-admin/";
            // var lcURL = "http://test02" + lcPort + "/servoy-admin/";
            //var lcURL = "http://localhost" + lcPort + "/servoy-admin/";

            string username = "update";
            string password = "update";
            string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{username}:{password}"));

            var client = new RestClient(lcURL);
            
            var request = new RestRequest("POST");

            request.AddHeader("Authorization", $"Basic {encoded}");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("rf", "Restart+Server%21");
            RestResponse response = client.Execute(request);
            HttpStatusCode statusCode = response.StatusCode;

            int numericStatusCode = (int)statusCode;

            if (numericStatusCode == 200)
            {
                if (Globals.Silent == false)
                {
                    txtbox.Text = txtbox.Text + DateTime.Now.ToString() + " - Server neugestartet" + "\n";
                    MessageBox.Show("Application Server wurde neugestartet", "Info", MessageBoxButtons.OK);
                }
            }
        }

        private Boolean restartService(string serv)
        {
            var llReturn = false;
            ServiceController sc = new ServiceController(serv);
            lblStatus.Text = "Servoy Dienst wird neugestartet...";
            sc.Stop();
            sc.WaitForStatus(ServiceControllerStatus.Stopped);

            sc.Start();
            sc.WaitForStatus(ServiceControllerStatus.Running);
            llReturn = true;
            return llReturn;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            chkMan.Checked = true;
            txtbox.Text = DateTime.Now.ToString() + " - Starte Updater" + "\n";
            cmdGetPort.PerformClick();
            cmdCheckUpdate.PerformClick();
            cmdRepoPaths.PerformClick();
            chkMan.Checked = false;
        }


        // --- Funktion welcher Port verwendet wird ---
        public string getPort()
        {
            string lcReturn = "FEHLER";
            string username = "update";
            string password = "update";
            string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{username}:{password}"));

            // var client = new RestClient("http://localhost:8080/servoy-admin/");
            //var client = new RestClient("http://test02:8080/servoy-admin/");

            var client = new RestClient(Globals.Host + ":8080/servoy-admin/");
            var request = new RestRequest("GET");
            
            request.Timeout = 2000;
            request.AddHeader("Authorization", $"Basic {encoded}");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            RestResponse response = client.Execute(request);
            HttpStatusCode statusCode = response.StatusCode;


            int numericStatusCode = (int)statusCode;

            if (numericStatusCode == 200)
            {
                lcReturn = ":8080";
            }
            else
            {   // --- Ist nicht die 8080, 8090 pingen ---
                //var client2 = new RestClient("http://localhost:8090/servoy-admin/");
                //var client2 = new RestClient("http://test02:8090/servoy-admin/");
                var client2 = new RestClient(Globals.Host + ":8090/servoy-admin/");

                var request2 = new RestRequest("GET");

                request2.Timeout = 2000;
                request2.AddHeader("Authorization", $"Basic {encoded}");
                request2.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                RestResponse response2 = client2.Execute(request2);
                HttpStatusCode statusCode2 = response2.StatusCode;

                int numericStatusCode2 = (int)statusCode2;
                if (numericStatusCode2 == 200)
                {
                    lcReturn = ":8090";
                }
                else
                { // --- Ist nicht die 8080 & 8090, 9090 nun pingen, wenns nicht klappt dann manuell eingeben ---
                    var client3 = new RestClient(Globals.Host + ":9090/servoy-admin/");

                    var request3 = new RestRequest("GET");

                    request3.Timeout = 2000;
                    request3.AddHeader("Authorization", $"Basic {encoded}");
                    request3.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                    RestResponse response3 = client3.Execute(request3);
                    HttpStatusCode statusCode3 = response3.StatusCode;

                    int numericStatusCode3 = (int)statusCode3;
                    if (numericStatusCode3 == 200)
                    {
                        lcReturn = ":9090";
                    }
                    else
                    {
                        { // --- Ist nicht die 8080 & 8090, 9090 nun pingen, wenns nicht klappt dann manuell eingeben ---
                            var client4 = new RestClient(Globals.Host + ":8070/servoy-admin/");

                            var request4 = new RestRequest("GET");

                            request4.Timeout = 2000;
                            request4.AddHeader("Authorization", $"Basic {encoded}");
                            request4.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                            RestResponse response4 = client4.Execute(request3);
                            HttpStatusCode statusCode4 = response4.StatusCode;

                            int numericStatusCode4 = (int)statusCode4;
                            if (numericStatusCode4 == 200)
                            {
                                lcReturn = ":8070";
                            }
                            else
                            {
                                lcReturn = "FEHLER";
                            }
                        }
                    }
                }
            }

            return lcReturn;
        }

        private void button2_Click(object sender, EventArgs e)
        {

            string lcPort = getPort();
            txtPort.Text = lcPort;
            if (lcPort == ":8080" || lcPort == ":8090" || lcPort == ":9090" || lcPort == ":8070")
            {
                txtbox.Text = txtbox.Text + DateTime.Now.ToString() + " - Port erfolgreich gefunden" + "\n";
            }
            else
            {
                MessageBox.Show("Port konnte nicht gefunden werden \nBitte von Hand eintragen.", "Fehler", MessageBoxButtons.OK);
                txtPort.ReadOnly = false;
                //return;
            }

            // --- URL anzeigen ---
            //linkURL.Text = "http://test02" + lcPort + "/servoy-admin/";
            linkURL.Text = Globals.Host + txtPort.Text + "/servoy-admin/";
            //linkURL.Text = "http://localhost" + lcPort + "/servoy-admin/";

            // --- PFADE HOLEN ---
            cmdRepoPaths_Click(new object(), new EventArgs());
            button3_Click(new object(), new EventArgs());

        }

        private void button3_Click(object sender, EventArgs e)
        {
            // --- Schauen ob ein Update bereit liegt auf C:\2goupd\UPD ---

            if (Directory.Exists(@"C:\2goup\UPD"))
            {
                DirectoryInfo d = new DirectoryInfo(@"C:\2goup\UPD");   //Assuming Test is your Folder
                FileInfo[] Files = d.GetFiles("*.*");                   //Getting Text files
                string str = "Folgende Updatedateien wurden gefunden:";
                foreach (FileInfo file in Files)
                {
                    str = str + "\n" + file.Name;
                }
                txtFileBox.Text = str;
                lblUpdate.Text = "Update ist verfügbar!";

            }
            else
            {
                txtFileBox.Text = "Update Ordner liegt nicht bereit, Update nicht möglich!";
                MessageBox.Show("Kein Update Ordner gefunden \n Bitte Installation prüfen.", "Fehler", MessageBoxButtons.OK);
            }
        }

        private void cmdRepoPaths_Click(object sender, EventArgs e)
        {
            var lcPort = txtPort.Text;
            //var lcURL       = "http://test02" + lcPort + "/servoy-admin/plugin-settings";
            //var lcURL     = "http://localhost" + lcPort + "/servoy-admin/plugin-settings";

            var lcURL = Globals.Host + lcPort + "/servoy-admin/plugin-settings";
            var lcVELO = "";
            var lcJASPER = "";
            var lcServoy = "";
            var lcDAT = "";

            string username = "update";
            string password = "update";
            string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{username}:{password}"));

            var client2 = new RestClient(lcURL);
            var request2 = new RestRequest("",RestSharp.Method.Get);

            request2.Timeout = 3000;
            request2.AddHeader("Authorization", $"Basic {encoded}");
            request2.AddHeader("Content-Type", "text/html");

            RestResponse response2 = client2.Execute(request2);

            string lcBody = response2.Content;

            //txtFileBox.Text = lcBody;

            // --- Finde VELO Pfad ---
            var lnAT = lcBody.IndexOf("=\"velocityreport.reportfolder\"");
            if (lnAT < 0)
            {
                lcVELO = "FEHLER";
                lblVELOpath.Text = lcVELO;
                return;
            }

            lcVELO = lcBody.Substring(lnAT);
            lnAT = lcVELO.IndexOf("ue=\"") + 4;
            lcVELO = lcVELO.Substring(lnAT);
            lnAT = lcVELO.IndexOf("\"");
            lcVELO = lcVELO.Substring(0, lnAT);

            txtbox.Text = txtbox.Text + DateTime.Now.ToString() + " - Velocity Pfad gefunden \n";

            // --- Finde JASPER Pfad ---
            lnAT = lcBody.IndexOf("=\"directory.jasper.report\"");
            if (lnAT < 0)
            {
                lcJASPER = "FEHLER";
                lblJASPERpath.Text = lcJASPER;
                return;
            }

            lcJASPER = lcBody.Substring(lnAT);
            lnAT = lcJASPER.IndexOf("ue=\"") + 4;
            lcJASPER = lcJASPER.Substring(lnAT);
            lnAT = lcJASPER.IndexOf("\"");
            lcJASPER = lcJASPER.Substring(0, lnAT);

            txtbox.Text = txtbox.Text + DateTime.Now.ToString() + " - Jasper Pfad gefunden \n";

            // --- Finde Servoy Pfad ---
            lnAT = lcBody.IndexOf("=\"velocityreport.reportfolder\"");
            if (lnAT < 0)
            {
                lcServoy = "FEHLER";
                lblServoyPath.Text = lcServoy;
                return;
            }

            lcServoy = lcBody.Substring(lnAT);
            lnAT = lcServoy.IndexOf("ue=\"") + 4;
            lcServoy = lcServoy.Substring(lnAT);
            lnAT = lcServoy.ToLower().IndexOf("servoy");
            lcServoy = lcServoy.Substring(0, lnAT + 7);

            txtbox.Text = txtbox.Text + DateTime.Now.ToString() + " - Servoy Pfad gefunden \n";

            lblServoyPath.Text = lcServoy;
            lblVELOpath.Text = lcVELO;
            lblJASPERpath.Text = lcJASPER;




            // --- Finde DAT Pfad ---
            lnAT = lcBody.IndexOf("servoy.FileServerService.defaultFolder\"");
            if (lnAT < 0)
            {
                lcDAT = "FEHLER";
                lblDATpath.Text = lcDAT;
                return;
            }

            lcDAT = lcBody.Substring(lnAT);
            lnAT = lcDAT.IndexOf("ue=\"") + 4;
            lcDAT = lcDAT.Substring(lnAT);
            lnAT = lcDAT.ToLower().IndexOf("\">");
            lcDAT = lcDAT.Substring(0, lnAT) + "\\DAT\\";

            txtbox.Text = txtbox.Text + DateTime.Now.ToString() + " - DAT Pfad gefunden \n";

            lblServoyPath.Text = lcServoy;
            lblVELOpath.Text = lcVELO;
            lblJASPERpath.Text = lcJASPER;
            lblDATpath.Text = lcDAT;


        }

       

          private void cmdCopyFiles_Click(object sender, EventArgs e)
          {
            panWait.Visible = true;
            panWait.BringToFront();
            lblStatus.Visible = true;
            prgBar.Visible = true;
            lblStatus.Text = "Bitte warten...";
            Application.DoEvents();

            string sourceDir = @"C:\2goup\UPD";
            string destVELO = lblVELOpath.Text;
            string destJASPER = lblJASPERpath.Text;
            
            string[] veloList = Directory.GetFiles(sourceDir, "*.xhtml");

            prgBar.Maximum = veloList.Length;
            prgBar.Value = 0;

            foreach (string f in veloList)
            {
                string fName = f.Substring(sourceDir.Length + 1);
                File.Copy(Path.Combine(sourceDir, fName), Path.Combine(destVELO, fName), true);
                prgBar.Value += 1;
                txtbox.Text = txtbox.Text + "\n" + " - Kopiere " + Path.Combine(sourceDir, fName) + " nach " + Path.Combine(destVELO, fName);
                lblStatus.Text = "Update " + fName;
                Application.DoEvents();
            }
            txtbox.Text += "\n";

            // string[] jasperList = Directory.GetFiles(sourceDir, "*.jrxml");

            // --- Update 22.07.2019 / .exe auch in Jasper kopieren ---
            var jasperList = Directory.EnumerateFiles(sourceDir, "*.*", SearchOption.AllDirectories)
            .Where(s => s.EndsWith(".jrxml") || s.EndsWith(".exe"));

            prgBar.Maximum = Directory.GetFiles(sourceDir, "*.jrxml").Length + Directory.GetFiles(sourceDir, "*.exe").Length;
            prgBar.Value = 0;


            foreach (string f in jasperList)
            {
                string fName = f.Substring(sourceDir.Length + 1);
                File.Copy(Path.Combine(sourceDir, fName), Path.Combine(destJASPER, fName), true);
                prgBar.Value += 1;
                txtbox.Text = txtbox.Text + "\n" + DateTime.Now.ToString() + " - Kopiere " + Path.Combine(sourceDir, fName) + " nach " + Path.Combine(destJASPER, fName);
                lblStatus.Text = "Update " + fName;
                Application.DoEvents();
            }

            File.Copy(Path.Combine(sourceDir, "eastwood.war"), Path.Combine(lblServoyPath.Text + "\\application_server\\server\\webapps\\", "eastwood.war"));

            if (Globals.Silent == false && 1 == 2)        // nur im normalen Updater wegen Rechte-Bug (Update 21.12.2021) // deaktiviert am 28.04.2022 => SW told so 
            {
                // --- UPDATE 2.4 - LOG4J JAR austauschen ---
                var jarList = Directory.EnumerateFiles(sourceDir, "*.*", SearchOption.AllDirectories)
                .Where(s => s.EndsWith(".jar"));

                // --- delete old jars in /lib/ ---
                var libPath = lblServoyPath.Text + "\\application_server\\lib\\";

                string[] oldJarFile = { "log4j-api.jar", 
                                        "log4j-core.jar", 
                                        "log4j-slf4j-impl.jar", 
                                        "log4j-web-2.11.2.jar" };

                foreach (string selectedOldJar in oldJarFile)
                {
                    File.Delete(libPath + selectedOldJar);
                }

                // --- copy the new jars into \lib ---
                foreach (string jars in jarList)
                {
                    string fName_jar = jars.Substring(sourceDir.Length + 1);
                    File.Copy(Path.Combine(sourceDir, fName_jar), Path.Combine(libPath, fName_jar), true);
                    txtbox.Text = txtbox.Text + "\n" + DateTime.Now.ToString() + " - Kopiere " + Path.Combine(sourceDir, fName_jar) + " nach " + Path.Combine(libPath, fName_jar);
                    lblStatus.Text = "Update " + fName_jar;
                    Application.DoEvents();
                }
            }
        }

        private void cmdUpdate_Click(object sender, EventArgs e)
        {
            string sourceDir = @"C:\2goup\UPD";
            string[] updateList = Directory.GetFiles(sourceDir, "*.servoy");

            if (updateList == null || updateList.Length == 0)
            {
                if (Globals.Silent == true)
                {
                    // --- ERROR LOG ablegen --
                    string filepath = @"C:\2goup\ERROR.LOG";
                    using (StreamWriter sw = File.CreateText(filepath))
                    {
                        sw.WriteLine("FEHLER : UPDATE KONNTE NICHT GEFUNDEN WERDEN");
                    }
                    Application.Exit();
                    
                }
                else
                {
                    MessageBox.Show("Keine Servoy Dateien gefunden", "Fehler", MessageBoxButtons.OK);
                    return;
                }

            }
            //button1_Click_2(new object(), new EventArgs()); // --- BACKUP HVO/DAT


            panWait.Visible = true;
            panWait.BringToFront();
            lblStatus.Visible = true;
            prgBar.Maximum = updateList.Length;
            prgBar.Value = 0;
            lblStatus.Text = "Bitte warten...";
            Application.DoEvents();
            foreach (string f in updateList)
            {
                string fName = f.Substring(sourceDir.Length + 1);
                txtbox.Text = txtbox.Text + "\n" + DateTime.Now.ToString() + " - Servoy Datei: " + Path.Combine(sourceDir, fName) + " gefunden";

                lblStatus.Text = "Lade " + fName + " hoch...";
                Application.DoEvents();

                // --- Upload each File ---

                var lcPort = txtPort.Text;
                //var lcURL = "http://test02" + lcPort + "/servoy-admin/solutions/import";
                //var lcURL = "http://localhost" + lcPort + "/servoy-admin/solutions/import";

                prgBar.Value += 1;

                var lcURL = Globals.Host + lcPort + "/servoy-admin/solutions/import";

                byte[] bytes = File.ReadAllBytes(Path.Combine(sourceDir, fName));

                string username = "update";
                string password = "update";
                string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{username}:{password}"));

                var client2 = new RestClient(lcURL);
                var request2 = new RestRequest("POST");

                request2.AddHeader("Authorization", $"Basic {encoded}");
                
                request2.AddFile("if", bytes, fName, "application/octet-stream");

                request2.AddParameter("ac", "on");
                request2.AddParameter("os", "on");
                request2.AddParameter("og", "on");
                request2.AddParameter("newname", "");
                request2.AddParameter("solution_password", "");
                request2.AddParameter("ak", "on");
                request2.AddParameter("md", "on");
                request2.AddParameter("up", "0");
                request2.AddParameter("dm", "on");
                request2.AddParameter("submit", "Import!");

                var response = client2.Execute(request2);

                if (fName == "HVO_G5.servoy")
                {
                    System.Threading.Thread.Sleep(35000);
                }

                HttpStatusCode statusCode = response.StatusCode;
                int numericStatusCode = (int)statusCode;

                if (numericStatusCode == 200)
                {
                    txtbox.Text = txtbox.Text + "\n" + DateTime.Now.ToString() + " - Servoy Datei: " + Path.Combine(sourceDir, fName) + " erfolgreich hochgeladen";
                }
                else
                {

                    if (Globals.Silent == true)
                    {
                        // --- ERROR LOG ablegen --
                        string filepath = @"C:\2goup\ERROR.LOG";
                        using (StreamWriter sw = File.CreateText(filepath))
                        {
                            sw.WriteLine("FEHLER : HVO_G5 KONNTE NICHT HOCHGELADEN WERDEN");
                        }
                        Application.Exit();
                    }
                    else
                    {

                        MessageBox.Show("Fehler beim hochladen von:" + fName, "Fehler", MessageBoxButtons.OK);
                        lblStatus.Visible = false;
                        panWait.Visible = false;
                        return;
                    }


                }
                // --- Free RestSharp Class ---
                client2 = null;
                request2 = null;

            }
            //lblStatus.Visible = false;
            //panWait.Visible = false;

            // --- Textlog ablegen ---
            string lcServoy = lblServoyPath.Text + "udpate_log/";
            //string lcFile = lcServoy + DateTime.Now.ToString() + "_log";
            string lcDate = DateTime.Now.ToShortDateString();
            lcDate = lcDate.Replace(@"/", @"\");
            lcDate = lcDate.Replace(" ", "");
            lcDate = lcDate.Replace(".", "");
            lcDate = lcDate.Replace(":", "");
            string lcFile = lcServoy + lcDate + "_log.txt";

            if (!Directory.Exists(lcServoy))
            {
                DirectoryInfo lcUpdate_LogDir = Directory.CreateDirectory(lcServoy);
            }

            File.WriteAllText(lcFile, txtbox.Text);

            //if (Globals.Silent == false)
            //{
             //   MessageBox.Show("Das HVO2go Update wurde erfolgreich installiert", "Erfolg", MessageBoxButtons.OK);
            //}
        }

        private void chkMan_CheckedChanged(object sender, EventArgs e)
        {
            cmdRestartServer.Enabled = chkMan.Checked;
            cmdGetPort.Enabled = chkMan.Checked;
            cmdCheckUpdate.Enabled = chkMan.Checked;
            cmdRepoPaths.Enabled = chkMan.Checked;
            cmdCopyFiles.Enabled = chkMan.Checked;
            cmdUpdate.Enabled = chkMan.Checked;
            cmdSQL.Enabled = chkMan.Checked;
            cmdGetPort.Visible = chkMan.Checked;

        }

        private void cmdStart_Click(object sender, EventArgs e)
        {
            if (txtPort.Text == "FEHLER")
            {
                MessageBox.Show("Daten sind fehlerhaft\nUpdate nicht gestartet!");
                return;
            }

            chkMan.Checked = true;
            panWait.Visible = true;
            lblStatus.Visible = true;
            lblStatus.Text = "Update wird ausgeführt, bitte warten...";
            prgBar.Visible = true;
            panWait.BringToFront();
            lblStatus.BringToFront();
            prgBar.BringToFront();
            Application.DoEvents();

            button3_Click(new object(), new EventArgs());   // --- CHECK UPDATE 
            button1_Click_2(new object(), new EventArgs()); // --- BACKUP HVO/DAT
            cmdUpdate_Click(new object(), new EventArgs()); // --- RUN UPDATE
            // --- Stop Servoy Serivce ---
            StopServoyService();
            cmdCopyFiles_Click(new object(), new EventArgs());  // --- COPY FILES + jars replacement

            if (AddLine() == false)// --- ADD EXTRA LINE FOR LONG FUNCTIONS (Update 15.07.2020)
            {
                MessageBox.Show("Konnte DISABLESCRIPTCOMPILE nicht anfügen!");
            }

            // --- update log4j vulnerability fix ---
            if (AddLineLOG4J() == false)// --- ADD EXTRA LINE FOR LONG FUNCTIONS (Update 15.07.2020)
            {
                MessageBox.Show("Konnte LOG4J SERVER Fix nicht anfügen!");
            }

            if (AddLineLOG4J_WRAPPER() == false)// --- ADD EXTRA LINE FOR LONG FUNCTIONS (Update 14.12.2021)
            {
                MessageBox.Show("Konnte LOG4J WRAPPER Fix nicht anfügen!");
            }

            if (AddLineEastwood() == false)// --- ADD EXTRA LINE FOR Eastwood (Update 14.12.2021)
            {
                MessageBox.Show("Konnte Eastwood nicht anfügen!");
            }

            // --- start servoy service ---
            StartServoyService();

            //button1_Click(new object(), new EventArgs());   // --- RESTART SERVER 
            chkMan.Checked = false;
            cmdStart.Enabled = false;
            lblUpdate.Text = "Update installiert!";
            panWait.Visible = false;

            MessageBox.Show("Das HVO2go Update wurde erfolgreich installiert", "Erfolg", MessageBoxButtons.OK);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            // System.Diagnostics.Process.Start("C:\\2goup\\fixbu.exe", lblDATpath.Text);


            OleDbConnection conn = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + lblDATpath.Text + ";Extended Properties=DBASE IV;");
            conn.Open();
            string sqlStr = "Select * from Clients.dbf";
            DataSet
                        //Make a DataSet object
                        myDataSet = new DataSet();
            OleDbDataAdapter
                        //Using the OleDbDataAdapter execute the query
                        myAdapter = new OleDbDataAdapter(sqlStr, conn);
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            // -------------------------------------------
            // --- Datensicherung aus Classic erzeugen ---
            // -------------------------------------------
            panWait.Visible = true;
            lblStatus.Text = "Sichere HVO-Datenbank...";
            Application.DoEvents();
            String lcDatPath = this.lblDATpath.Text;
            String lcBackupPath = "";
            String lcBackupTemp = "";
            String lcZIPname = @"\db_backup";

            // --- Datenpfad prüfen ---
            if (lcDatPath.Length != 0)
            {
                if (Directory.Exists(lcDatPath))
                {
                    int position = lcDatPath.IndexOf("\\DAT");
                    lcBackupPath = lcDatPath.Substring(0, position);
                    lcBackupPath = lcBackupPath + @"/DB_backup";
                    lcBackupTemp = lcBackupPath + @"/temp/";
                }
                else
                {
                    if (Globals.Silent == false)
                    {
                        MessageBox.Show("Konnte Classic Datenpfad nicht anpassen", "Fehler", MessageBoxButtons.OK);
                        return;
                    }
                }
            }

            // --- Backupfad prüfen und falls nötig erstellen ---
            if (lcBackupPath.Length != 0)
            {
                if (!Directory.Exists(lcBackupPath))
                {
                    Directory.CreateDirectory(lcBackupPath);
                }
            }
            // --- Sind schon 3 Backups vorhanden ? ---
            if (File.Exists(lcBackupPath + lcZIPname + "1.zip"))
            {
                if (File.Exists(lcBackupPath + lcZIPname + "2.zip"))
                {
                    if (File.Exists(lcBackupPath + lcZIPname + "3.zip"))
                    {
                        // --- File 3 Delete und Namen wegziehen ---
                        File.Delete(lcBackupPath + lcZIPname + "3.zip");
                        File.Move(lcBackupPath + lcZIPname + "2.zip", lcBackupPath + lcZIPname + "3.zip");
                        File.Move(lcBackupPath + lcZIPname + "1.zip", lcBackupPath + lcZIPname + "2.zip");

                        lcZIPname = lcZIPname + "1.zip";
                    }
                    else
                    {
                        lcZIPname = lcZIPname + "3.zip";
                    }
                }
                else
                {
                    lcZIPname = lcZIPname + "2.zip";
                }
            }
            else
            {
                lcZIPname = lcZIPname + "1.zip";
            }


            // --- DAT Ordner kopieren und umbennen ---
            //Now Create all of the directories
            if (Directory.Exists(lcBackupTemp))
            {
                Directory.Delete(lcBackupTemp, true);
            }

            Directory.CreateDirectory(lcBackupTemp);
            System.Threading.Thread.Sleep(3000);
            prgBar.Maximum = Directory.GetFiles(lcDatPath, "*.*").Length;
            prgBar.Value = 0;
            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(lcDatPath, "*.*", SearchOption.TopDirectoryOnly))
            {
                File.Copy(newPath, newPath.Replace(lcDatPath, lcBackupTemp), true);
                lblStatus.Text = "...sichere Datei: " + newPath;
                prgBar.Value += 1;
                Application.DoEvents();
            }

            // --- Backup Zippen --
            ZipFile.CreateFromDirectory(lcBackupTemp, lcBackupPath + lcZIPname);

            // --- Tempordner löschen ---
            Directory.Delete(lcBackupTemp, true);
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Globals.VersionsInfo);
        }

        private void label3_Click(object sender, EventArgs e)
        {
            //
        }

        private void button1_Click_3(object sender, EventArgs e)
        {
            var temp = AddLineLOG4J_WRAPPER();
        }

        private Boolean StopServoyService()
        {
            Boolean llReturn = false;

            ServiceController sc = new ServiceController("ServoyService");

            if ((sc.Status.Equals(ServiceControllerStatus.Stopped)) || (sc.Status.Equals(ServiceControllerStatus.StopPending)))
            {
                lblStatus.Text = "Servoy Dienst gestoppt";
                llReturn = true;
            }
            else
            {
                // Stop the service if its status is not set to "Stopped".
                lblStatus.Text = "Servoy Dienst wird gestoppt...";
                Application.DoEvents();
                try
                {
                    sc.Stop();
                    sc.WaitForStatus(ServiceControllerStatus.Stopped);
                    lblStatus.Text = "Servoy Dienst gestoppt";
                    llReturn = true;
                }
                catch (Exception X)
                {
                    MessageBox.Show("Konnte den Dienst nicht stoppen\nKeine Adminrechte?\nFehler:\n" + X.Message.ToString(), "Fehler");
                    lblStatus.Text = "Fehler beim stoppen vom Servoy Dienst";
                    llReturn = false;
                }


            }
            Application.DoEvents();

            return llReturn;
        }

        private Boolean StartServoyService()
        {
            Boolean llReturn = false;
            ServiceController sc = new ServiceController("ServoyService");

            if ((sc.Status.Equals(ServiceControllerStatus.Running)) || (sc.Status.Equals(ServiceControllerStatus.StartPending)))
            {
                lblStatus.Text = "Servoy Dienst läuft";
                llReturn = true;
            }
            else
            {
                // Stop the service if its status is not set to "Starting".
                lblStatus.Text = "Servoy Dienst wird gestartet";
                Application.DoEvents();
                try
                {
                    lblStatus.Text = "Servoy Dienst wird gestartet...";
                    sc.Start();
                    sc.WaitForStatus(ServiceControllerStatus.Running);
                    lblStatus.Text = "Servoy Dienst läuft";
                    llReturn = true;
                }
                catch (Exception X)
                {
                    MessageBox.Show("Konnte den Dienst nicht gestartet\nKeine Adminrechte?\nFehler:\n" + X.Message.ToString(), "Fehler");
                    lblStatus.Text = "Fehler";
                    llReturn = false;
                }


            }
            Application.DoEvents();

            return llReturn;
        }

    }
}
